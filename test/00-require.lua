#!/usr/bin/env lua

require 'Test.Assertion'

plan(16)

if not require_ok 'Rotas' then
    BAIL_OUT "no lib"
end

local m = require 'Rotas'
is_table( m )
equals( m, package.loaded.Rotas )

equals( m._NAME, 'Rotas', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'web server router', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

array_equals( m.http_methods, { 'DELETE', 'GET', 'HEAD', 'OPTIONS', 'PATCH', 'POST', 'PUT', 'TRACE' } )
is_string( m.matcher, 'Silva.template' )

local o = m()
is_table( o, 'instance' )

o = m{ 'foo', bar = 42 }
is_table( o )
equals( o[1], 'foo' )
equals( o.bar, 42 )
o.bar = 0
equals( o.bar, 0 )
o.baz = 33
equals( o.baz, 33 )
