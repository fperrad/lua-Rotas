#!/usr/bin/env lua

require 'Test.Assertion'

plan(6)

local rotas = require 'Rotas'

local mt = {}
function mt:foo ()
    print('# ' .. self.bar)
end
local o = setmetatable({ bar = 'FooBar' }, { __index = mt })

local app = rotas(o)
is_table( app.ALL )
equals( #app.ALL, 0 )
is_table( app )
is_string( app.bar )
is_function( app.foo )
not_error( function() app:foo() end )
