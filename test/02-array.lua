#!/usr/bin/env lua

require 'Test.Assertion'

if not pcall(require, 'Silva') then
    skip_all 'no Silva'
end

plan(8)

local sme = require 'Silva'
local rotas = require 'Rotas'

local app = rotas{}
app.GET[sme('/index.html', 'identity')] = function (path)
    return path
end
app.GET[sme('/foo%?query=(%d+)', 'lua')] = function (params)
    return params[1]
end

local fn, capture, res
fn, capture = app('GET', '/index.html')
is_function( fn )
res = fn(capture)
equals( res, '/index.html' )

fn, capture = app('GET', '/foo?query=42')
is_function( fn )
res = fn(capture)
equals( res, '42' )

fn, capture = app('GET', '/bar?query=42')
is_nil( fn )
is_nil( capture )

fn, capture = app('FOO', '/foo')
is_nil( fn )
is_nil( capture )

