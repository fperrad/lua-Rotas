#!/usr/bin/env lua

require 'Test.Assertion'

if not pcall(require, 'Silva') then
    skip_all 'no Silva'
end

plan(27)

local sme = require 'Silva'
local rotas = require 'Rotas'
rotas.http_methods = { 'GET', 'POST', 'PUT' }

local app = rotas{}
is_table( app.ALL )
equals( #app.ALL, 0 )
is_table( app.GET )
is_table( app.POST )
is_table( app.PUT )

app.GET[sme('/*.html', 'shell')] = function (path)
    return path
end
equals( #app.GET, 1)

app.GET[sme'/foo{?query}'] = function (params)
    return params.query
end
equals( #app.GET, 2)

app.ALL['/bar{?query}'] = function (params)
    return params.query
end
equals( #app.ALL, 0 )
equals( #app.GET, 3)
equals( #app.POST, 1)
equals( #app.PUT, 1)

error_matches( function () app.GET[42] = false end,
        "key not callable" )

error_matches( function () app.GET['/foo{=bad}'] = false end,
        "operator for future extension " )

error_matches( function () app.PATCH['/foo{?query}'] = false end,
        "attempt to index " )

local fn, capture, res
fn, capture = app('GET', '/index.html')
is_function( fn )
res = fn(capture)
equals( res, '/index.html' )

fn, capture = app('GET', '/foo?query=42')
is_function( fn )
res = fn(capture)
equals( res, '42' )

fn, capture = app('GET', '/bar?query=42')
is_function( fn )
res = fn(capture)
equals( res, '42' )

fn, capture = app('PUT', '/bar?query=42')
is_function( fn )
res = fn(capture)
equals( res, '42' )

fn, capture = app('GET', '/baz?query=42')
is_nil( fn )
is_nil( capture )

fn, capture = app('FOO', '/foo')
is_nil( fn )
is_nil( capture )

error_matches( function () app('ALL', '/index.html') end,
        "allowed only for registration" )
