
local livr = require'LIVR.Validator'

local function livr_helper (spec, fn)
    local validator = livr.new(spec)
    return function (capture)
        local params, err = validator:validate(capture)
        if err then
            return { errmsg = err }
        else
            return fn(params)
        end
    end
end

local uri = require'Silva.template'
local rotas = require'Rotas'
local calc = rotas()

calc.GET[uri'/calc/mul{?x,y}'] = livr_helper({
    x = { 'required', 'decimal' },
    y = { 'required', 'decimal' },
}, function (params)
    return { ret = params.x * params.y }
end)

calc.GET[uri'/calc/div{?x,y}'] = livr_helper({
    x = { 'required', 'decimal' },
    y = { 'required', 'decimal' },
}, function (params)
    if params.y == 0 then
        return { errmsg = "Division by zero" }
    end
    return { ret = params.x / params.y }
end)

return calc
