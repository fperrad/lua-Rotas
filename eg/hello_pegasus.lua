
local function handler (req, res, router)
    local meth = req:method()
    local url = req:path()
    print(meth, url)
    local fn, capture = router(meth, url)
    if not fn then
        res:statusCode(404)
    else
        res:statusCode(200)
        res:addHeader('Content-Type', 'text/plain')
        res:write(fn(capture))
    end
end

local pegasus = require'pegasus':new{
    host = 'localhost',
    port= 8080,
}

pegasus:start(function(req, res)
    handler(req, res, require'hello')
end)

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

]]
