#!/usr/bin/env lua

require 'Test.Assertion'

plan(10)

local calc = require'calc'

local fn, capture, res
fn, capture = calc('GET', '/calc/div?x=3.0&y=2.0')
res = fn(capture)
equals( res.ret, 1.5 )

fn, capture = calc('GET', '/calc/div?x=3.0&y=0.0')
res = fn(capture)
equals( res.errmsg, "Division by zero" )

fn, capture = calc('GET', '/calc/div?x=a&y=b')
res = fn(capture)
equals( res.errmsg.x, 'NOT_DECIMAL' )
equals( res.errmsg.y, 'NOT_DECIMAL' )

fn, capture = calc('GET', '/calc/div')
res = fn(capture)
equals( res.errmsg.x, 'REQUIRED' )
equals( res.errmsg.y, 'REQUIRED' )

fn, capture = calc('GET', '/calc/idiv')
is_nil( fn )
is_nil( capture )

fn, capture = calc('GET', '/calc/div?z=top')
is_nil( fn )
is_nil( capture )
