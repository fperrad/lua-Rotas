
local http_server = require'http.server'
local http_headers = require'http.headers'
local encode = require'dkjson'.encode

local function json_fmt (stream, req_method, obj, status)
    local res_headers = http_headers.new()
    res_headers:append('access-control-allow-origin', '*')
    if status then
        res_headers:append(':status', status)
    elseif obj and obj.errmsg then
        res_headers:append(':status', '400')    -- Bad Request
    elseif req_method == 'POST' then
        res_headers:append(':status', '201')    -- Created
    else
        res_headers:append(':status', '200')    -- OK
    end
    if obj then
        res_headers:append('cache-control', 'max-age=0, must-revalidate, no-cache, no-store, private')
        res_headers:append('content-type', 'application/json')
        res_headers:append('x-content-type-options', 'nosniff')
        stream:write_headers(res_headers, false)
        stream:write_body_from_string(encode(obj))
    else
        stream:write_headers(res_headers, true)
    end
end

local function handler (_, stream, router, formatter)
    local req_headers = stream:get_headers()
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    print(req_method, req_path)
    local fn, capture = router(req_method, req_path)
    if not fn then
        formatter(stream, req_method, { errmsg = '404 Not Found' }, '404')
    else
        formatter(stream, req_method, fn(capture))
    end
end

local myserver = http_server.listen{
    host = 'localhost',
    port = '8080',
    onstream = function (server, stream) handler(server, stream, require'calc', json_fmt) end,
    onerror = function (myserver, context, op, err, errno)      -- luacheck: ignore 212
        local msg = op .. ' on ' .. tostring(context) .. ' failed'
        if err then
            msg = msg .. ': ' .. tostring(err)
        end
        io.stderr:write(msg, "\n")
    end,
}

assert(myserver:listen())
io.stderr:write(string.format("Now listening on port %d\n", select(3, myserver:localname())))
assert(myserver:loop())

--[[

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}
    $ curl "http://localhost:8080/calc/div?x=3.0&y=0.0"
    {"errmsg":"Division by zero"}
    $ curl "http://localhost:8080/calc/div?x=a&y=b"
    {"errmsg":{"y":"NOT_DECIMAL","x":"NOT_DECIMAL"}}
    $ curl "http://localhost:8080/calc/div"
    {"errmsg":{"y":"REQUIRED","x":"REQUIRED"}}
    $ curl "http://localhost:8080/calc/idiv"
    {"errmsg":"404 Not Found"}
    $ curl "http://localhost:8080/calc/div?z=top"
    {"errmsg":"404 Not Found"}

]]
