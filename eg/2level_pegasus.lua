
local encode = require'dkjson'.encode
local re = require'Silva.lua'
local rotas = require'Rotas'
local app = rotas()

local function json_fmt (res, meth, obj, status)
    res:addHeader('Access-Control-Allow-Origin', '*')
    if status then
        res:statusCode(status)
    elseif obj and obj.errmsg then
        res:statusCode(400)     -- Bad Request
    elseif meth == 'POST' then
        res:statusCode(201)     -- Created
    else
        res:statusCode(200)     -- OK
    end
    if obj then
        res:addHeader('Content-Type', 'application/json')
        res:write(encode(obj))
    end
end

app.ALL[re'^/calc/'] = function (req, res)
    local meth = req:method()
    local url = req:path()
    if req._query_string and req._query_string ~= '' then
        url = url .. '?' .. req._query_string
    end
    print(meth, url)
    local fn, capture = require'calc'(meth, url)
    if not fn then
        json_fmt(res, meth, { errmsg = '404 Not Found' }, 404)
    else
        json_fmt(res, meth, fn(capture))
    end
end

app.ALL[re'^/hello'] = function (req, res)
    local meth = req:method()
    local url = req:path()
    print(meth, url)
    local fn, capture = require'hello'(meth, url)
    if not fn then
        res:statusCode(404)
    else
        res:statusCode(200)
        res:addHeader('Content-Type', 'text/plain')
        res:write(fn(capture))
    end
end

app.ALL[re'%.html$'] = function (req, res)
    local meth = req:method()
    local url = req:path()
    print(meth, url)
    if meth == 'GET' then
        local filename = '.' .. url
        local file = io.open(filename, 'rb')
        if file then
            res:writeFile(file, 'text/html')
        else
            res:statusCode(404)
        end
    else
        res:statusCode(405)
    end
end

local pegasus = require'pegasus':new{
    host = 'localhost',
    port= 8080,
}

pegasus:start(function(req, res)
    local hdl = app(req:method(), req:path())
    if not hdl then
        res:statusCode(404)
    else
        hdl(req, res)
    end
end)

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}

    $ x-www-browser http://localhost:8080/calc.html

]]
