
local xavante = require'xavante'
local err_404 = require'xavante.httpd'.err_404

local function handler (req, res, router)
    print(req.cmd_mth, req.cmd_url)
    local fn, capture = router(req.cmd_mth, req.cmd_url)
    if not fn then
        return err_404(req, res)
    else
        res.statusline = 'HTTP/1.1 200 OK'
        res.headers['Content-Type'] = 'text/plain'
        res.content = fn(capture)
        return res
    end
end

xavante.HTTP{
    server = { host = '*', port = 8080 },
    defaultHost = {
        rules = {
            {
                match = '^/hello',
                with = function (req, res) return handler(req, res, require'hello') end,
            },
            {
                match = '^/alo(.*)',
                with = require 'xavante.redirecthandler',
                params = { '/hello%1' }
            }
        }
    }
}

xavante.start()

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

    $ curl "http://localhost:8080/alo/bob"
    Hello, bob

]]
