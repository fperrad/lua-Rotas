
local encode = require'cjson'.encode

local function json_fmt (meth, obj, status)
    ngx.header['Access-Control-Allow-Origin'] = '*'
    if status then
        ngx.status = status
    elseif obj and obj.errmsg then
        ngx.status = ngx.HTTP_BAD_REQUEST
    elseif meth == 'POST' then
        ngx.status = ngx.HTTP_CREATED
    else
        ngx.status = ngx.HTTP_OK
    end
    if obj then
        ngx.header['Content-Type'] = 'application/json'
        ngx.say(encode(obj))
    end
end

local method = ngx.req.get_method()
local fn, capture = require'calc'(method, ngx.var.request_uri)
if not fn then
    json_fmt(method, { errmsg = '404 Not Found' }, ngx.HTTP_NOT_FOUND)
else
    json_fmt(method, fn(capture))
end

--[[

    $ nginx -p `pwd`/ -c conf/nginx.conf

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}
    $ curl "http://localhost:8080/calc/div?x=3.0&y=0.0"
    {"errmsg":"Division by zero"}
    $ curl "http://localhost:8080/calc/div?x=a&y=b"
    {"errmsg":{"y":"NOT_DECIMAL","x":"NOT_DECIMAL"}}
    $ curl "http://localhost:8080/calc/div"
    {"errmsg":{"y":"REQUIRED","x":"REQUIRED"}}
    $ curl "http://localhost:8080/calc/idiv"
    {"errmsg":"404 Not Found"}
    $ curl "http://localhost:8080/calc/div?z=top"
    {"errmsg":"404 Not Found"}

]]
