
local encode = require'dkjson'.encode

local function json_fmt (res, meth, obj, status)
    res:addHeader('Access-Control-Allow-Origin', '*')
    if status then
        res:statusCode(status)
    elseif obj and obj.errmsg then
        res:statusCode(400)     -- Bad Request
    elseif meth == 'POST' then
        res:statusCode(201)     -- Created
    else
        res:statusCode(200)     -- OK
    end
    if obj then
        res:addHeader('Content-Type', 'application/json')
        res:write(encode(obj))
    end
end

local function handler (req, res, router, formatter)
    local meth = req:method()
    local url = req:path()
    if req._query_string and req._query_string ~= '' then
        url = url .. '?' .. req._query_string
    end
    print(meth, url)
    local fn, capture = router(meth, url)
    if not fn then
        formatter(res, meth, { errmsg = '404 Not Found' }, 404)
    else
        formatter(res, meth, fn(capture))
    end
end

local pegasus = require'pegasus':new{
    host = 'localhost',
    port= 8080,
}

pegasus:start(function(req, res)
    handler(req, res, require'calc', json_fmt)
end)

--[[

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}
    $ curl "http://localhost:8080/calc/div?x=3.0&y=0.0"
    {"errmsg":"Division by zero"}
    $ curl "http://localhost:8080/calc/div?x=a&y=b"
    {"errmsg":{"y":"NOT_DECIMAL","x":"NOT_DECIMAL"}}
    $ curl "http://localhost:8080/calc/div"
    {"errmsg":{"y":"REQUIRED","x":"REQUIRED"}}
    $ curl "http://localhost:8080/calc/idiv"
    {"errmsg":"404 Not Found"}
    $ curl "http://localhost:8080/calc/div?z=top"
    {"errmsg":"404 Not Found"}

]]
