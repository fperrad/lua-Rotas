
local encode = require'dkjson'.encode
local re = require'Silva.lua'
local rotas = require'Rotas'
local app = rotas()

local function json_fmt (req, res, obj, status)
    res.headers['Access-Control-Allow-Origin'] = '*'
    if status then
        res.statusline = 'HTTP/1.1 ' .. status
        obj = obj or { errmsg = status }
    elseif obj and obj.errmsg then
        res.statusline = 'HTTP/1.1 400 Bad Request'
    elseif req.cmd_mth == 'POST' then
        res.statusline = 'HTTP/1.1 201 Created'
    else
        res.statusline = 'HTTP/1.1 200 OK'
    end
    if obj then
        res.headers['Content-Type'] = 'application/json'
        res.content = encode(obj)
    end
    return res
end

app.ALL[re'^/calc/'] = function (req, res)
    print(req.cmd_mth, req.cmd_url)
    local fn, capture = require'calc'(req.cmd_mth, req.cmd_url)
    if not fn then
        return json_fmt(req, res, nil, '404 Not Found')
    else
        return json_fmt(req, res, fn(capture))
    end
end

local err_404 = require'xavante.httpd'.err_404

app.ALL[re'^/hello'] = function (req, res)
    print(req.cmd_mth, req.cmd_url)
    local fn, capture = require'hello'(req.cmd_mth, req.cmd_url)
    if not fn then
        return err_404(req, res)
    else
        res.statusline = 'HTTP/1.1 200 OK'
        res.headers['Content-Type'] = 'text/plain'
        res.content = fn(capture)
        return res
    end
end

local filehandler = require'xavante.filehandler'{ baseDir = '.' }
app.ALL[re'%.html$'] = function (req, res)
    print(req.cmd_mth, req.cmd_url)
    return filehandler(req, res)
end

local redirecthandler = require'xavante.redirecthandler'.makeHandler{ '/hello%1' }
app.ALL[re'^/alo(.*)'] = function (req, res, capture)
    print(req.cmd_mth, req.cmd_url)
    return redirecthandler(req, res, capture)
end

local xavante = require'xavante'

xavante.HTTP{
    server = { host = '*', port = 8080 },
    defaultHost = {
        rules = {
            {
                match = '^/',
                with = function (req, res)
                    local hdl, capture = app(req.cmd_mth, req.cmd_url)
                    if not hdl then
                        err_404(req, res)
                    else
                        return hdl(req, res, capture)
                    end
                end,
            },
        }
    }
}

xavante.start()

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}

    $ x-www-browser http://localhost:8080/calc.html

    $ curl "http://localhost:8080/alo/bob"
    Hello, bob

]]
