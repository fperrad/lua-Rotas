
local rotas = require'Rotas'
local app = rotas()

app.GET['/hello'] = function ()
    return "Hello, World"
end

app.GET['/hello{/name}'] = function (params)
    return "Hello, " .. params.name or ''
end

return app
