
local http_server = require'http.server'
local http_headers = require'http.headers'
local encode = require'dkjson'.encode
local re = require'Silva.lua'
local rotas = require'Rotas'
local app = rotas()

local function json_fmt (stream, req_method, obj, status)
    local res_headers = http_headers.new()
    res_headers:append('access-control-allow-origin', '*')
    if status then
        res_headers:append(':status', status)
    elseif obj and obj.errmsg then
        res_headers:append(':status', '400')    -- Bad Request
    elseif req_method == 'POST' then
        res_headers:append(':status', '201')    -- Created
    else
        res_headers:append(':status', '200')    -- OK
    end
    if obj then
        res_headers:append('cache-control', 'max-age=0, must-revalidate, no-cache, no-store, private')
        res_headers:append('content-type', 'application/json')
        res_headers:append('x-content-type-options', 'nosniff')
        stream:write_headers(res_headers, false)
        stream:write_body_from_string(encode(obj))
    else
        stream:write_headers(res_headers, true)
    end
end

app.ALL[re'^/calc/'] = function (stream, req_headers)
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    print(req_method, req_path)
    local fn, capture = require'calc'(req_method, req_path)
    if not fn then
        json_fmt(stream, req_method, { errmsg = '404 Not Found' }, '404')
    else
        json_fmt(stream, req_method, fn(capture))
    end
end

app.ALL[re'^/hello'] = function (stream, req_headers)
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    print(req_method, req_path)
    local fn, capture = require'hello'(req_method, req_path)
    local res_headers = http_headers.new()
    if not fn then
        res_headers:append(':status', '404')
        stream:write_headers(res_headers, true)
    else
        local txt = fn(capture)
        res_headers:append(':status', '200')
        res_headers:append('cache-control', 'max-age=0, must-revalidate, no-cache, no-store, private')
        res_headers:append('content-type', 'text/plain')
        res_headers:append('x-content-type-options', 'nosniff')
        stream:write_headers(res_headers, false)
        stream:write_body_from_string(txt)
    end
end

app.ALL[re'%.html$'] = function (stream, req_headers)
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    print(req_method, req_path)
    local res_headers = http_headers.new()
    if req_method == 'GET' then
        local filename = '.' .. req_path
        local file = io.open(filename, 'rb')
        if file then
            res_headers:append(':status', '200')
            res_headers:append('cache-control', 'max-age=0, must-revalidate, no-cache, no-store, private')
            res_headers:append('content-type', 'text/html')
            res_headers:append('x-content-type-options', 'nosniff')
            res_headers:append('x-frame-options', 'sameorigin')
            stream:write_headers(res_headers, false)
            stream:write_body_from_file(file)
        else
            res_headers:append(':status', '404')
            stream:write_headers(res_headers, true)
        end
    else
        res_headers:append(':status', '405')
        stream:write_headers(res_headers, true)
    end
end

app.ALL[re'^/alo'] = function (stream, req_headers)
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    local uri = req_headers:get':scheme' .. '://' .. req_headers:get':authority' .. req_path:gsub('^/alo', '/hello')
    print(req_method, req_path)
    local res_headers = http_headers.new()
    res_headers:append(':status', '301')
    res_headers:append('location', uri)
    stream:write_headers(res_headers, true)
end

local myserver = http_server.listen{
    host = 'localhost',
    port = '8080',
    onstream = function (server, stream)        -- luacheck: ignore 212
        local req_headers = stream:get_headers()
        local hdl = app(req_headers:get':method', req_headers:get':path')
        if not hdl then
            local res_headers = http_headers.new()
            res_headers:append(':status', '404')
            stream:write_headers(res_headers, true)
        else
            hdl(stream, req_headers)
        end
    end,
    onerror = function (server, context, op, err, errno)        -- luacheck: ignore 212
        local msg = op .. ' on ' .. tostring(context) .. ' failed'
        if err then
            msg = msg .. ': ' .. tostring(err)
        end
        io.stderr:write(msg, "\n")
    end,
}

assert(myserver:listen())
io.stderr:write(string.format("Now listening on port %d\n", select(3, myserver:localname())))
assert(myserver:loop())

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}

    $ x-www-browser http://localhost:8080/calc.html

    $ curl -L "http://localhost:8080/alo/bob"
    Hello, bob

]]
