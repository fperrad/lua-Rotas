
local http_server = require'http.server'
local http_headers = require'http.headers'

local function handler (_, stream, router)
    local req_headers = stream:get_headers()
    local req_method = req_headers:get':method'
    local req_path = req_headers:get':path'
    print(req_method, req_path)
    local fn, capture = router(req_method, req_path)
    local res_headers = http_headers.new()
    if not fn then
        res_headers:append(':status', '404')
        stream:write_headers(res_headers, true)
    else
        local txt = fn(capture)
        res_headers:append(':status', '200')
        res_headers:append('cache-control', 'max-age=0, must-revalidate, no-cache, no-store, private')
        res_headers:append('content-type', 'text/plain')
        res_headers:append('x-content-type-options', 'nosniff')
        stream:write_headers(res_headers, false)
        stream:write_body_from_string(txt)
    end
end

local myserver = http_server.listen{
    host = 'localhost',
    port = '8080',
    onstream = function (server, stream) handler(server, stream, require'hello') end,
    onerror = function (myserver, context, op, err, errno)      -- luacheck: ignore 212
        local msg = op .. ' on ' .. tostring(context) .. ' failed'
        if err then
            msg = msg .. ': ' .. tostring(err)
        end
        io.stderr:write(msg, "\n")
    end,
}

assert(myserver:listen())
io.stderr:write(string.format("Now listening on port %d\n", select(3, myserver:localname())))
assert(myserver:loop())

--[[

    $ curl "http://localhost:8080/hello"
    Hello, World
    $ curl "http://localhost:8080/hello/bob"
    Hello, bob

    $ curl --insecure "https://localhost:8080/hello/alice"
    Hello, alice

]]
