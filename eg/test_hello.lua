#!/usr/bin/env lua

require 'Test.Assertion'

plan(4)

local app = require'hello'

local fn, capture, res
fn, capture = app('GET', '/hello')
res = fn(capture)
equals( res, "Hello, World" )

fn, capture = app('GET', '/hello/bob')
res = fn(capture)
equals( res, "Hello, bob" )

fn, capture = app('GET', '/ola')
is_nil( fn )
is_nil( capture )
