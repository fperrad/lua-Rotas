
local xavante = require'xavante'
local encode = require'dkjson'.encode

local function json_fmt (req, res, obj, status)
    res.headers['Access-Control-Allow-Origin'] = '*'
    if status then
        res.statusline = 'HTTP/1.1 ' .. status
        obj = obj or { errmsg = status }
    elseif obj and obj.errmsg then
        res.statusline = 'HTTP/1.1 400 Bad Request'
    elseif req.cmd_mth == 'POST' then
        res.statusline = 'HTTP/1.1 201 Created'
    else
        res.statusline = 'HTTP/1.1 200 OK'
    end
    if obj then
        res.headers['Content-Type'] = 'application/json'
        res.content = encode(obj)
    end
    return res
end

local function handler (req, res, router, formatter)
    print(req.cmd_mth, req.cmd_url)
    local fn, capture = router(req.cmd_mth, req.cmd_url)
    if not fn then
        return formatter(req, res, nil, '404 Not Found')
    else
        return formatter(req, res, fn(capture))
    end
end

xavante.HTTP{
    server = { host = '*', port = 8080 },
    defaultHost = {
        rules = {
            {
                match = '^/calc/',
                with = function (req, res) return handler(req, res, require'calc', json_fmt) end,
            },
            {
                match = '.html$',
                with = require 'xavante.filehandler',
                params = { baseDir = '.' }
            }
        }
    }
}

xavante.start()

--[[

    $ curl "http://localhost:8080/calc/div?x=3.0&y=2.0"
    {"ret":1.5}
    $ curl "http://localhost:8080/calc/div?x=3.0&y=0.0"
    {"errmsg":"Division by zero"}
    $ curl "http://localhost:8080/calc/div?x=a&y=b"
    {"errmsg":{"y":"NOT_DECIMAL","x":"NOT_DECIMAL"}}
    $ curl "http://localhost:8080/calc/div"
    {"errmsg":{"y":"REQUIRED","x":"REQUIRED"}}
    $ curl "http://localhost:8080/calc/idiv"
    {"errmsg":"404 Not Found"}
    $ curl "http://localhost:8080/calc/div?z=top"
    {"errmsg":"404 Not Found"}

    $ x-www-browser http://localhost:8080/calc.html

]]
