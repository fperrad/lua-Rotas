
# lua-Rotas

---

## Overview

lua-Rotas is a web server router,
designed to work with [lua-Silva](https://fperrad.frama.io/lua-Silva),
_your personal string matching expert_.

## Status

lua-Rotas is now stable.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-Rotas source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-Rotas).

## Installation

lua-Rotas is available via LuaRocks:

```sh
luarocks install lua-rotas
```

lua-Rotas is available via opm:

```sh
opm get fperrad/lua-rotas
```

or manually, with:

```sh
make install
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/).

    make test

## Copyright and License

Copyright &copy; 2018-2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.
