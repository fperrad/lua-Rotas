
lua-Rotas : a web server router
===============================

Introduction
------------

lua-Rotas is a web server router,
designed to work with [lua-Silva](https://fperrad.frama.io/lua-Silva),
_your personal string matching expert_.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-Rotas>,
and the sources are hosted at <https://framagit.org/fperrad/lua-Rotas>.

Copyright and License
---------------------

Copyright (c) 2018-2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

